window.addEventListener('load', function () {
    let tbody = document.querySelector('tbody')
    function init() {
        axios({
            url: '/score/list'
        }).then(res => {
            let htmlStr = ``

            let data = res.data.data
            for (let key in data) {
                htmlStr += `<tr>
                                <th scope="row" >${key}</th>
                                <td>${data[key].name}</td>
                                <td class="score" data-id='${key}' data-batch='1'>${data[key].score[0]}</td>
                                <td class="score" data-id='${key}' data-batch='2'>${data[key].score[1]}</td>
                                <td class="score" data-id='${key}' data-batch='3'>${data[key].score[2]}</td>
                                <td class="score" data-id='${key}' data-batch='4'>${data[key].score[3]}</td>
                                <td class="score" data-id='${key}' data-batch='5'>${data[key].score[4]}</td>
                            </tr>`
            }
            tbody.innerHTML = htmlStr
        })
    }
    init()

    // 实现成绩的录入和修改
    tbody.addEventListener('click', function (e) {
        if (e.target.classList.contains('score')) {
            let td = e.target
            // 将原先的内容先存起来
            let score = td.innerText
            // 判断是否有input标签，有则不触发
            if (!td.querySelector('input')) {
                // 清空原先td的内容
                td.innerText = ``
                // 创建一个input输入框
                let input = document.createElement('input')
                // 追加到当前td中
                td.appendChild(input)
                // 为input赋值，值就是td中的原始的内容（上面已经存好了，直接用）
                input.value = score
                // 为输入框设置样式：去除边框-去除默认颜色-文字居中
                // 去除原本的边框
                input.style.border = 'none'
                // 去除默认背景色
                input.style.background = 'transparent'
                // 去除边框
                input.style.outline = 'none'
                // 文字居中
                input.style.textAlign = 'center'
                // 让输入框聚焦
                // input.focus()
                input.select()

                // 声明变量用于传递参数
                let stu_id = e.target.dataset.id
                let batch = e.target.dataset.batch
                // 为输入框添加一个失焦事件
                input.addEventListener('blur', function () {
                    // 并没有修改，所以将原值改回去
                    td.innerText = score
                })
                // 触发回车确认修改事件
                input.addEventListener('keyup', function (e) {
                    // 判断触发的按键为回车
                    if (e.keyCode == 13) {
                        // if判断输入值的范围
                        if (input.value < 0 || input.value > 100) {
                            toastr.warning('分数范围必须0-100')
                            input.addEventListener('blur', function () {
                                // 并没有修改，所以将原值改回去
                                td.innerText = score
                            })
                            return
                        }
                        score = input.value
                        // 将修改的值赋值给传递的参数上
                        let data = { score, stu_id, batch }
                        // 发起修改请求
                        axios({
                            method: 'post',
                            url: '/score/entry',
                            data
                        }).then(res => {
                            // 判断成功修改后
                            if (res.data.code == 0) {
                                console.log('触发回车确认事件');
                                toastr.success(res.data.message)
                                init()
                                // 触发失焦事件后，将修改后的值赋值到表单中，代替重新渲染，避免影响聚焦失焦事件
                                // input.addEventListener('blur', function () {
                                //     td.innerText = input.value
                                // })
                            }
                        })
                    }
                })
            }
        }
    })
})

