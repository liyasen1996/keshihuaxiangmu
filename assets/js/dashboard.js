window.addEventListener('load', function () {
    let total = document.querySelector('.total')
    let avgSalary = document.querySelector('.avgSalary')
    let avgAge = document.querySelector('.avgAge')
    let proportion = document.querySelector('.proportion')

        ; (function () {
            axios({
                url: '/student/overview'
            }).then(res => {
                console.log(res);
                let data = res.data.data
                total.innerText = data.total
                avgSalary.innerText = data.avgSalary
                avgAge.innerText = data.avgAge
                proportion.innerText = data.proportion
            })
        })()

    // 学生数据
    function init() {
        axios({
            url: '/student/list'
        }).then(res => {
            console.log(res);
            let data = res.data.data
            // 从数据源中提取出饼图所需要的数据
            let pieArr = []

            // 声明折线图所需的数组
            let names = [], salarys = [], truesalarys = []


            data.forEach(function (value) {
                // 提取饼图数据
                // 判断一个pirArr中是否已经有了当前省份的记录，如果有，数量+1
                // let obj = pirArr.filter(function(v){
                // return v.name == value.province
                // })[0]

                // 声明一个空对象
                let obj = null
                // 利用for循环，判断数据中的省份是否出现过
                for (let i = 0; i < pieArr.length; i++) {
                    // 如果出现过
                    if (pieArr[i].name == value.province) {
                        // 则为obj赋值
                        obj = pieArr[i]
                        break
                    }
                }
                // 如果obj有值，则为ture，执行+1，为次数加一次
                if (obj) {
                    obj.value++
                }
                // 如果obj为空，则说明数据中没有出现过此省份，则新增进去，且赋值value为1
                else {
                    // 此对象数组的格式为生成图标锁要求的
                    pieArr.push({ value: 1, name: value.province })
                }
                // 提取饼图数据结束

                // 提取折线图数据开始
                names.push(value.name)
                salarys.push(value.salary)
                truesalarys.push(value.truesalary)

                // 提取折线图数据结束


            })
            // 调用生成饼图的函数
            createPie(pieArr)



            // 调用生成折线图的函数
            createLine(names, salarys, truesalarys)
        })
    }
    init()

    // 学生成绩
    function d(params) {

    }

    // 生成静态饼图
    function createPie(data) {
        // 基于准备好的dom，初始化echarts实例
        let myChart = echarts.init(document.querySelector('.pie'));
        // 绘制图表
        let option = {
            title: {
                text: 'Nightingale Chart',
                left: 'left'
            },
            tooltip: {
                trigger: 'item',
                formatter: '{a} <br/>{b} : {c} ({d}%)'
            },
            series: [
                {
                    name: 'Nightingale Chart',
                    type: 'pie',
                    radius: [25, 120],
                    center: ['50%', '50%'],
                    roseType: 'area',
                    itemStyle: {
                        borderRadius: 20
                    },
                    data
                }
            ]
        };
        myChart.setOption(option)
    }

    // 生成静态折线图
    function createLine(names, salarys, truesalarys) {
        let mychart = echarts.init(document.querySelector('.line'))

        option = {
            dataZoom: [
                {
                    start: 0,
                    end: 60
                }
            ],
            title: {
                text: 'Stacked Line'
            },
            tooltip: {
                trigger: 'axis'
            },
            legend: {
                data: ['预计薪资', '实际薪资']
            },
            xAxis: {
                type: 'category',
                data: names
            },
            yAxis: {
                type: 'value'
            },
            series: [
                {
                    name: '预计薪资',
                    data: salarys,
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    lineStyle: {
                        color: `blue`
                    }
                },
                {
                    name: '实际薪资',
                    data: truesalarys,
                    type: 'line',
                    smooth: true,
                    symbol: 'none',
                    lineStyle: {
                        color: `red`
                    },
                }
            ]
        };

        mychart.setOption(option)
    }

    // 生成柱状静态图
    function createLine() {

    }
})