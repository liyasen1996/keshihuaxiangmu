window.addEventListener('load', function () {
    // 获取初始化按钮
    let init = document.querySelector('.init')
    // 为初始化按钮绑定事件并发起请求，获得初始化数据
    init.addEventListener('click', function () {
        axios({
            url: '/init/data',
        }).then(res => {
            console.log(res);
        })
    })
})