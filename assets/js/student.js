window.addEventListener('load', function () {
    //必须在请求头中传递token
    //请求头的键名必须为  Authorization
    let htmlStr
    let tbody = document.querySelector('tbody')
    // 获取添加学员按钮元素
    let btnAdddiglog = document.querySelector('.btnAdddiglog')

    //获取数据修改回填所需的元素
    let name = document.querySelector('[name="name"]')
    let age = document.querySelector('[name="age"]')
    let group = document.querySelector('[name="group"]')
    let sexs = document.querySelectorAll('[name="sex"]')
    let phone = document.querySelector('[name="phone"]')
    let salary = document.querySelector('[name="salary"]')
    let truesalary = document.querySelector('[name="truesalary"]')
    // 获取省市区级联的元素
    let province = document.querySelector('[name="province"]')
    let city = document.querySelector('[name="city"]')
    let county = document.querySelector('[name="county"]')
    // 获取新增信息模态框内form表单元素
    let addForm = document.querySelector('.add-form')
    // 获取模态框重置按钮
    let btnreset = document.querySelector('.btnreset')
    // 拿到编辑新增模态框的标题及按钮元素
    let btnOpt = document.querySelector('.btnOpt')
    let dialogTitle = document.querySelector('.dialogTitle')
    // 实现数据渲染
    function init() {
        axios({
            url: 'student/list',
            // 利用本地存储拿到token，传给服务器验证
            headers: { Authorization: localStorage.getItem('token_123') }
        }).then(res => {
            if (res.data.code == 0) {
                htmlStr = ''
                res.data.data.forEach(function (value, index) {
                    htmlStr += `<tr>
                                    <th scope="row">${index + 1}</th>
                                    <td>${value.name}</td>
                                    <td>${value.age}</td>
                                    <td>${value.sex}</td>
                                    <td>${value.group}</td>
                                    <td>${value.phone}</td>
                                    <td>${value.salary}</td>
                                    <td>${value.truesalary}</td>
                                    <td>${value.province}${value.city}${value.county}</td>
                                    <td>
                                    <button type="button" 
                                    class="btn btn-primary btn-sm btnedit"
                                    data-id='${value.id}'
                                    >修改</button>
                                    <button type="button" 
                                    class="btn btn-danger btn-sm btn-del"
                                    data-id='${value.id}'
                                    >删除</button>
                                    </td>
                                </tr>`
                })
                tbody.innerHTML = htmlStr
            }
            if (res.data.code == 0) {
                toastr.success('列表刷新')
            }
        })
    }
    init()

    // 实现省市区联动
    // 请求省的数据并渲染到模态框省级中
    axios({
        url: 'geo/province'
    }).then(res => {
        let htmlStr = '<option selected value="">--省--</option>'
        res.data.forEach(function (value) {
            htmlStr += `<option value="${value}">${value}</option>`
        })
        province.innerHTML = htmlStr
    })

    // 实现市级联动
    province.addEventListener('change', function () {
        county.innerHTML = `<option selected value="">--县/区--</option>`
        if (!province.value) {
            city.innerHTML = `<option selected value="">--市--</option>`
            return
        }
        axios({
            url: '/geo/city',
            params: { pname: province.value }
        }).then(res => {
            let htmlStr = `<option selected value="">--市--</option>`
            res.data.forEach(function (value) {
                htmlStr += `<option value="${value}">${value}</option>`
            })
            city.innerHTML = htmlStr
        })
    })
    // 实现区级联动
    city.addEventListener('change', function () {
        // 默认省市为空，则区不必获取数据
        if (!province.value) {
            return
        }
        if (!city.value) {
            return
        }
        axios({
            url: '/geo/county',
            params: { pname: province.value, cname: city.value }
        }).then(res => {
            let htmlStr = `<option selected value="">--县/区--</option>`
            res.data.forEach(function (value) {
                htmlStr += `<option value="${value}">${value}</option>`
            })
            county.innerHTML = htmlStr
        })
    })

    // 实现数据新增,呼出模态框
    btnAdddiglog.addEventListener('click', function () {
        $('#addModal').modal('show')
        // 模态框内容修改
        dialogTitle.innerHTML = '录入新学员'
        btnOpt.innerHTML = '确认添加'
        btnreset.click()
    })

    // 添加数据的校验
    function student() {
        return {
            fields: {
                name: {
                    validators: {
                        notEmpty: {
                            message: '姓名不能为空',
                        },
                        stringLength: {
                            min: 2,
                            max: 10,
                            message: '姓名长度2~10位'
                        }
                    }
                },
                age: {
                    validators: {
                        notEmpty: {
                            message: '年龄不能为空',
                        },
                        greaterThan: {
                            value: 18,
                            message: '年龄不能小于18岁'
                        },
                        lessThan: {
                            value: 60,
                            message: '年龄不能超过60岁'
                        }
                    }
                },
                sex: {
                    validators: {
                        choice: {
                            min: 1,
                            max: 1,
                            message: '请选择性别'
                        }
                    }
                },
                group: {
                    validators: {
                        notEmpty: {
                            message: '组号不能为空',
                        },
                        regexp: {
                            regexp: /^\d{1,2}$/,
                            message: '请选择有效的组号'
                        }
                    }
                },
                phone: {
                    validators: {
                        notEmpty: {
                            message: '手机号不能为空',
                        },
                        regexp: {
                            regexp: /^1\d{10}$/,
                            message: '请填写有效的手机号'
                        }
                    }
                },
                salary: {
                    validators: {
                        notEmpty: {
                            message: '实际薪资不能为空',
                        },
                        greaterThan: {
                            value: 800,
                            message: '期望薪资不能低于800'
                        },
                        lessThan: {
                            value: 100000,
                            message: '期望薪资不能高于100000'
                        }
                    }
                },
                truesalary: {
                    validators: {
                        notEmpty: {
                            message: '实际薪资不能为空',
                        },
                        greaterThan: {
                            value: 800,
                            message: '实际薪资不能低于800'
                        },
                        lessThan: {
                            value: 100000,
                            message: '实际薪资不能高于100000'
                        }
                    }
                },
                province: {
                    validators: {
                        notEmpty: {
                            message: '省份必填',
                        },
                    }
                },
                city: {
                    validators: {
                        notEmpty: {
                            message: '市必填',
                        },
                    }
                },
                county: {
                    validators: {
                        notEmpty: {
                            message: '县必填',
                        },
                    }
                },
            }
        }
    }

    let editId
    // 实现新增数据操作---或编辑操作
    $('.add-form').bootstrapValidator(student()).on('success.form.bv', function (e) {
        e.preventDefault();
        // 为添加或编辑设置判断条件
        let formdata = new FormData(addForm)
        let data = {}
        formdata.forEach(function (value, key) {
            data[key] = value
        })
        if (dialogTitle.innerHTML == '录入新学员') {
            axios({
                method: 'POST',
                url: '/student/add',
                data
            }).then(res => {
                if (res.data.code == 0) {
                    console.log(res);
                    btnreset.click()
                    toastr.success(res.data.message)
                    $('#addModal').modal('hide')
                    init()
                } else if (res.data.code == 1) {
                    toastr.info('添加失败，请重试')
                }
            })
        }
        // 做编辑请求数据功能并渲染
        else {
            data.id = editId
            axios({
                method: 'PUT',
                url: '/student/update',
                data
            }).then(res => {
                console.log(res);
                if (res.data.code == 0) {
                    console.log(res);
                    btnreset.click()
                    toastr.success(res.data.message)
                    $('#addModal').modal('hide')
                    init()
                } else if (res.data.code == 1) {
                    toastr.info('添加失败，请重试')
                }
            })
        }
    })

    // 实现数据删除
    tbody.addEventListener('click', function (e) {
        if (e.target.classList.contains('btn-del')) {
            if (confirm('确认删除吗')) {
                let id = e.target.dataset.id
                console.log(id);
                axios({
                    url: '/student/delete',
                    method: 'DELETE',
                    params: { id }
                }).then(res => {
                    console.log(res);
                    if (res.data.code == 0) {
                        toastr.success('学员信息删除成功')
                        init()
                    }
                })
            }
        }
        // 实现数据的回填--模态框的弹出--
        if (e.target.classList.contains('btnedit')) {
            $('#addModal').modal('show')
            let id = e.target.dataset.id
            editId = id
            axios({
                url: '/student/one',
                params: { id }
            }).then(res => {
                console.log(res);
                console.log(res.data.data.city);
                let data = res.data.data
                // 常规表单元素
                name.value = data.name
                age.value = data.age
                phone.value = data.phone
                salary.value = data.salary
                truesalary.value = data.truesalary
                // 单选框表单元素
                data.sex == '男' ? (sexs[0].checked = true) : (sexs[1].checked = true)
                // 下拉菜单元素
                group.value = data.group
                province.value = data.province


                // ********未解决
                // 触发省下拉列表的change事件
                // createEvent：创建一个事件
                // HTMLEvents：页面元素事件
                let evt1 = document.createEvent('HTMLEvents')
                // initEvent：事件初始化  initEvent（事件类型，是否冒泡，是否阻止默认行为）
                evt1.initEvent('change', false, false)
                // dispatchEvent：触发事件，用需要触发事件的元素调用
                province.dispatchEvent(evt1)
                setTimeout(() => {
                    city.value = data.city
                    let evt2 = document.createEvent('HTMLEvents')
                    evt2.initEvent('change', false, false)
                    city.dispatchEvent(evt2)
                    setTimeout(() => {
                        county.value = data.county
                    }, 500)
                }, 500)

                // 模态框内容修改
                dialogTitle.innerHTML = '编辑学员'
                btnOpt.innerHTML = '确认编辑'
            })
        }
    })

    // 实现数据修改
    // 实现数据的回填--模态框的弹出--



})