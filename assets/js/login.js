window.addEventListener('load', function () {
    // 去注册元素
    let goRegister = document.querySelector('.goRegister')
    // 去登入
    let goLogin = document.querySelector('.goLogin')
    // 注册面板
    let register = document.querySelector('.register')
    // 登入面板
    let login = document.querySelector('.login')
    // 1.5 获取注册面板中的用户名和密码元素
    let rusername = document.querySelector('.register [name=username]')
    let rpassword = document.querySelector('.register [name=password]')
    // 1.6 获取登陆面板中的用户名和密码元素
    let lusername = document.querySelector('.login [name=username]')
    let lpassword = document.querySelector('.login [name=password]')


    // 绑定去注册点击事件，呼出注册面板
    goRegister.addEventListener('click', function () {
        register.style.display = 'block'
        login.style.display = 'none'
    })

    // 绑定去登录点击事件，呼出登录面板
    goLogin.addEventListener('click', function () {
        login.style.display = 'block'
        register.style.display = 'none'
    })

    // 绑定事件，用于注册用户且发起请求给服务器传输注册信息

    // 用于验证注册信息的工具函数 test
    function test() {
        return {
            // 指定需要验证的字段名称
            fields: {
                // 这里username是 input 的name属性值，表示对这个输入框进行验证
                username: {
                    // 添加真正的校验规则
                    validators: {
                        //不能为空
                        notEmpty: {
                            message: '用户名不能为空.'// 如果不满足校验规则，则给出这句提示
                        },
                        //检测长度
                        stringLength: {
                            min: 2, // 最少2位
                            max: 15, // 最多15位
                            message: '用户名需要2~15个字符'
                        }
                    }
                },
                // 这里password是 input 的name属性值，表示对这个输入框进行验证
                password: {
                    // 添加真正的校验规则
                    validators: {
                        // 不能为空
                        notEmpty: {
                            //空时的 提示信息
                            message: '密码不能为空'
                        },
                        // 内容长度监测
                        stringLength: {   //检测长度
                            min: 6,
                            max: 15,
                            message: '密码需要6~15个字符'
                        }
                    }
                }
            }
        }
    }

    // $('.register form') ：表示的是指定监听那个表单的submit默认提交行为
    // bootstrapValidator ：调用插件的验证方法
    // test() ： 校验规则，要自己定义
    // success.form.bv ：sumbit的默认提交行为--固定写死
    $('.register form').bootstrapValidator(test()).on('success.form.bv', function (e) {
        // 验证通过后，会在这里继续执行（验证是test（））
        e.preventDefault();
        let username = rusername.value
        let password = rpassword.value
        axios({
            url: '/api/register',
            method: 'post',
            data: { username, password }
        }).then(res => {
            console.log(res.data);
            if (res.data.code == 0) {
                toastr.success('恭喜！注册成功！')
                // 注册成功后，清空原信息且返回登录页面,且将新注册的帐号密码赋值到登录界面。
                lusername.value = rusername.value
                lpassword.value = rpassword.value
                rusername.value = rpassword.value = ''
                goLogin.click()
            } else if (res.data.code == 1) {
                toastr.info('您注册的账号已存在，请重新输入')
            }
        })
    });

    // 登录页面的基本实现
    $('.login form').bootstrapValidator(test()).on('success.form.bv', function (e) {
        // 验证通过后，会在这里继续执行（验证是test（））
        e.preventDefault();
        let username = lusername.value
        let password = lpassword.value
        axios({
            url: '/api/login',
            method: 'post',
            data: { username, password }
        }).then(res => {
            if (res.data.code == 0) {
                toastr.success('恭喜！登录成功！')
                // 注册成功后，清空原信息且返回登录页面,且将新注册的帐号密码赋值到登录界面。
                localStorage.setItem('token_123', res.data.token)
                location.href = './index.html'
                console.log(res.data);
            } else if (res.data.code == 1) {
                toastr.nfo('您输入的账号或密码错误，请重新输入')
                console.log(res.data);
            }
        })
    });
})