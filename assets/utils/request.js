// 全局配置根路径
axios.defaults.baseURL = 'http://www.itcbc.com:8000'

// axios.defaults.headers.common['Authorization'] = localStorage.getItem('token_123')

// 添加请求拦截器：所有的请求都会经过
// config ： 请求报文对象
// request： 请求
axios.interceptors.request.use(
    function (config) {
        // 在发送请求之前做些什么：以请求头的方式传递token
        let token = localStorage.getItem('token_123')
        // 判断是否有token，如果有token才进行传递
        if (token) {
            config.headers.Authorization = token
        }
        console.log('确认请求经过此拦截器');
        return config
    },
    function (error) {
        // 对请求错误做些什么
        return Promise.reject(error)
    }
)